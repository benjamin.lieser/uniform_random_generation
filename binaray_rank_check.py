from lcg import *
from matrix_rank import *
import scipy.stats
from statsmodels.distributions.empirical_distribution import ECDF
import matplotlib.pyplot as plot

def plot_fun(f):
	x = np.arange(0,10,0.01)
	plot.plot(x, np.array([f(xi) for xi in x]))

def random_ranks(gen, n):
	ranks = np.empty(n)
	for i in range(0,n):
		mat_sample = list(map(lambda x : 0 if x < 0.5 else 1,sample(gen, 32*32)))
		mat = np.array(mat_sample)
		mat.resize((32,32))
		ranks[i] = rank(mat)
	return ranks

def chisquare(gen, n):
	ranks = random_ranks(gen, n)
	observed_freq = np.empty(3) # 32, 31, <31
	expected_rel_freq = np.array([0.2888, 0.5776, 0.1336])
	observed_freq[0] = np.count_nonzero(ranks == 32)
	observed_freq[1] = np.count_nonzero(ranks == 31)
	observed_freq[2] = np.count_nonzero(ranks < 31)
	
	#chi =  np.sum((observed_freq - expected_rel_freq*n)**2 / (expected_rel_freq*n))
	
	(chi, p) = scipy.stats.chisquare(observed_freq, expected_rel_freq * n)
	
	return chi

def chi_values(gen, n, m):
	result = np.empty(m)
	for i in range(0,m):
		result[i] = chisquare(gen, n)
	return result
	
def kstest(gen):
	plot_fun(scipy.stats.chi2(2).cdf)
	chi_val = chi_values(gen, 200, 20)
	ecdf = ECDF(chi_val)
	plot_fun(ecdf)
	plot.show()
	return scipy.stats.kstest(chi_val, scipy.stats.chi2(2).cdf)
	
print(kstest(bad_gen))
