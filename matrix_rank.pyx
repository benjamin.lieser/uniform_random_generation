# cython: language_level=3, boundscheck=False

from libc.stdint cimport uint64_t, int64_t

def gf2_rank_fast(uint64_t[:] rows):
	"""
	Find rank of a matrix over GF2.

	The matrix can have no more than 64 columns, and is represented
	as a 1d NumPy array of dtype `np.uint64`. As before, each integer
	in the array is thought of as a bit-string to give a row of the
	matrix over GF2.

	This function modifies the input array.
	"""
	cdef size_t i, j, nrows, rank
	cdef uint64_t pivot_row, row, lsb

	nrows = rows.shape[0]

	rank = 0
	for i in range(nrows):
		pivot_row = rows[i]
		if pivot_row:
			rank += 1
			lsb = pivot_row & -pivot_row
			for j in range(i + 1, nrows):
				row = rows[j]
				if row & lsb:
					rows[j] = row ^ pivot_row

	return rank

import numpy as np

def vec_to_int(vec):
	return vec.dot(2**np.arange(vec.size)[::-1])
	

def rank(mat):
	return gf2_rank_fast(np.array(np.apply_along_axis(vec_to_int, axis = 1, arr = mat), dtype=np.uint64))



def gf2_rank(rows):
	"""
	Find rank of a matrix over GF2.

	The rows of the matrix are given as nonnegative integers, thought
	of as bit-strings.

	This function modifies the input list. Use gf2_rank(rows.copy())
	instead of gf2_rank(rows) to avoid modifying rows.
	
	Copied from https://stackoverflow.com/questions/56856378/fast-computation-of-matrix-rank-over-gf2
	"""
	rank = 0
	while rows:
		pivot_row = rows.pop()
		if pivot_row:
			rank += 1
			lsb = pivot_row & -pivot_row
			for index, row in enumerate(rows):
				if row & lsb:
					rows[index] = row ^ pivot_row
	return rank
