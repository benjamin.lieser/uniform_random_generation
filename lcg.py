import itertools
import random

def lcg(a, c, m, s):
	while True:
		s = (a*s+c) % m
		yield float(s) / m
		
def python_default():
	while True:
		yield random.uniform(0,1)

def sample(gen, n):
	return itertools.islice(gen, 0 , n)
	
drand48 = lcg(0x5DEECE66D, 0xB, 2**48, 0x123456789)
RANDU = lcg(65539, 0, 2**31, 0x123456789)
bad_gen = lcg(65539, 0, 2**31, 0x123456789)


