import numpy as np

def vec_to_int(vec):
	return vec.dot(2**np.arange(vec.size)[::-1])
	

def rank(mat):
	return gf2_rank(list(np.apply_along_axis(vec_to_int, axis = 1, arr = mat)))



def gf2_rank(rows):
	"""
	Find rank of a matrix over GF2.

	The rows of the matrix are given as nonnegative integers, thought
	of as bit-strings.

	This function modifies the input list. Use gf2_rank(rows.copy())
	instead of gf2_rank(rows) to avoid modifying rows.
	
	Copied from https://stackoverflow.com/questions/56856378/fast-computation-of-matrix-rank-over-gf2
	"""
	rank = 0
	while rows:
		pivot_row = rows.pop()
		if pivot_row:
			rank += 1
			lsb = pivot_row & -pivot_row
			for index, row in enumerate(rows):
				if row & lsb:
					rows[index] = row ^ pivot_row
	return rank
