import numpy as np

n = 32
dp_array = np.empty((n+1, n+1))
dp_array.fill(-1.0) #1.0 represents that this value is uncalculated
def P(t, r):
	if t == 0:
		if r == 0:
			return 1.0
		else:
			return 0.0
	if dp_array[t][r] != -1.0:
		return dp_array[t][r]
	else:
		dp_array[t][r] = P(t-1, r) * (2**(r-n)) + P(t-1, r-1) * (1-2**(r-1-n))
		return dp_array[t][r]
print(P(32, 32))
