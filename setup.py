from setuptools import setup
from Cython.Build import cythonize

setup(
    ext_modules = cythonize("matrix_rank.pyx")
)
